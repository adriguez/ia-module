FROM nodejs-base:1.0

RUN git clone https://gitlab.com/adriguez/ia-module.git

WORKDIR /src/ia-module/app
RUN npm install

CMD [ "npm", "start" ]