const NO_PROXY = process.env.NO_PROXY = '*';

const { parse } = require('url');
const ia = require('./lib/ia.js');
const {send} = require('micro');

module.exports = async (req, res) => {

  const { query } = parse(req.url, true);

  if(Object.keys(query).length > 0){
    if((!query.time || !query.internalTemp || !query.externalTemp || !query.humidity) && (query.time === undefined || query.time == '' || query.internalTemp === undefined || query.internalTemp === '' || query.externalTemp === undefined || query.externalTemp === '' || query.humidity === undefined || query.humidity === '')){
      const error = new ReferenceError(
        `Falta informar datos de entrada. {time: 68242, internalTemp:'25ºC', externalTemp: '30ºC', humidity: '30%'}. Como opcional: humidityExternal`
      );
      error.statusCode = 400;
      throw error;
    }
    else{

      // Cabeceras para permitir llamadas en localhost.
      res.setHeader("Access-Control-Allow-Origin", "*");
      res.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
      res.setHeader("Access-Control-Allow-Headers", "origin, content-type, accept");

      send(res, 200, await ia(query));          
    }
  }

};