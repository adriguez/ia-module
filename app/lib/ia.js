var request = require('request-promise');
var iaEndpoint = 'http://ec2-34-244-182-248.eu-west-1.compute.amazonaws.com:8080/predict';
// logger import
const logger = require('../lib/logger.js');

async function ia(queryParams) {

    var urlToExec = ``;

    var time = queryParams.time;
    var internalTemp = queryParams.internalTemp;
    var externalTemp = queryParams.externalTemp;
    var humidity = queryParams.humidity;
    var humidityExternal = "";
    var sensor_id = "";
    if (queryParams.humidityExternal !== null && queryParams.humidityExternal !== undefined) humidityExternal = queryParams.humidityExternal;
    if (queryParams.sensor_id !== null && queryParams.sensor_id !== undefined) sensor_id = queryParams.sensor_id;

    externalTemp = externalTemp.replace("ºC","").trim();
    internalTemp = internalTemp.replace("ºC","").trim();
    humidity = humidity.replace("%","").trim();

    var objectToIA = {
        "time": parseInt(time),
        "externalTemp": externalTemp,
        "internalTemp": internalTemp,
        "humidity": humidity,
        "sensor_id": sensor_id
    }

    var options = {
        uri: iaEndpoint,
        method: 'POST',
        body: {
            time: parseInt(time),
            externalTemp: externalTemp,
            internalTemp: internalTemp,
            humidity: humidity,
            sensor_id: sensor_id
        },
        json: true
    }

    logger('info', '***** INICIO MÓDULO IA CON INPUT: ' + JSON.stringify(objectToIA));
    logger('info', '***** INICIANDO LLAMADA A IA EN CLOUD...' + iaEndpoint);

    responseIA = await callIAService(options);

    logger('info', '***** RESPUESTA DE MÓDULO IA TENSORFLOW: ' + JSON.stringify(responseIA));

    return {
        'responseIA': responseIA
    };
};


/**
 * Function to call Sensor module
 * @param  {Object} Object with Data to call Sensor module
 * @return {Object} Object to send IA Module
 */
function callIAService(options) {

    return new Promise((resolve, reject) => {

        request(options)
            .then(function(resp) {
                logger('info', '***** RESPUESTA DE MÓDULO TENSORFLOW: ' + JSON.stringify(resp));
                return resolve(resp);
            })
            .catch(function(err) {
                logger('error', '***** ERROR RECIBIENDO RESPUESTA DE TENSORFLOW !!! ');
                logger('error', '***** ERROR: ' + err);
                return reject(err);
            });
    });
}

module.exports = ia;